��    #      4  /   L           	          +  �  ;    �     �  
   �               -     >     Y     h     m  
   z  
   �     �     �     �  >   �  %   �  
         +     9  '   B     j     s     �     �     �  l   �  *        ?     H  �  g     �	     
     
  �  *
  @  �     >     Q     ]  #   e     �  #   �     �     �     �     �     �          1     Q  N   T  0   �  
   �     �     �  &   
     1     H     _     g     �  y   �  .   �     .     7                                                  !            
      "   #                    	                                                                  <b>General</b> <b>Information</b> <b>Security</b> <b>What is uAdBlock and how it works?</b><p>uAdBlock is an adblocker based on host lists. These lists are downloaded and anchored in the system. Every system queries a domain first for an IP address in its own system, so it is possible to redirect these requests to the localhost and prevent the pages from being loaded. It does not block anything in the narrower sense, but does not loading it at all.</p> <b>Where do the lists come from?</b><p>The lists come from different projects like StevenBlack, Energized and AdGuard. At regular intervals, these lists are updated and anyone can use and collaborate for free. Already a large number of users have contributed to these lists.</p> Activate uAdBlock Blocklists Cancel Check for updates on app start? Disable uAdBlock Enable push notifications? Enter password Help Last updated Maintainer No Update! No lists selected! No update available. OK Please note that this app will modify your readonly filesystem Please, select a block list to start! Push Token Set new lists Settings Should the system remain in r/w status? Unblock! Update uAdBlock Version Welcome to uAdBlock 2.0 Yes You start this app for the first time. We will look for files from the old version and make a full clean up. Your password is required for this action: uAdBlock uAdBlock has been deactivated! Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-04-22 05:51+0200
Last-Translator: Malte Kiefer <malte.kiefer@mailgermania.de>
Language-Team: German <https://translate-ut.org/projects/uadblock/uadblock/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 3.0.1
 <b>Allgemein</b> <b>Intomationen</b> <b>Sicherheit</b> <b>Was ist uAdBlock und wie funktioniert es?</b><p>uAdBlock ist ein Adblocker der auf Host Listen basiert. Diese Listen werden heruntergeladen und im System verankert. Ein jedes System prüft beim Abfragen einer Domain erst im eigenen System nach einer IP Adresse, daher ist es möglich diese Anfragen auf den Localhost umzuleiten und so zu verhindern, dass die Seiten geladen werden. Es wir als im engeren Sinne nichts blockiert, sondern erst gar nicht geladen.</p> <b>Woher kommen die Listen?</b><p>Die Listen kommen aus verschiedenen Projekten wie StevenBlack, Energized and AdGuard. In regelmäßigen Abständen werden diese Listen aktualisiert und jeder kann diese Listen kostenlos nutzen und mitarbeiten. Bereits eine große Anzahl an Nutzer hat zu diesen Listen mitgearbeitet.</p> Aktiviere uAdBlock Blocklisten Abbruch Überprüfe auf Updates beim Start? Deaktiviere uAdBlock Push-Benachrichtigungen aktivieren? Passwort eingeben Hilfe Letzte Aktualisierung Betreuer Kein Aktualisierung vorhanden! Keine Liste ausgewählt! Kein Aktualisierung verfügbar. OK Bitte beachten Sie, dass diese App Ihr schreibgeschütztes Dateisystem ändert Bitte, wählen Sie eine Blocklist vor dem Start! Push-Token Neue Listenkombination laden Einstellungen Soll das System im r/w Status bleiben? uAdBlock deaktivieren! uAdBlock aktualisieren Version Willkommen zu uAdBlock 2.0 Ja Sie starten diese App zum ersten Mal. Wir überprüfen das System auf Dateien von einer alten Version und löschen diese. Ihr Passwort wird für diese Aktion benötigt: uAdBlock uAdBlock wurde deaktiviert! 